<?php

namespace Application\Model;

class Miesiace
{
    public function pobierzWszystkie()
    {
        return [
            'blue' => 'Styczeń',
            'yellow' => 'Luty',
            'red' =>'Marzec',
            'orange' =>'Kwiecień',
            'green' =>'Maj',
            'purple'=>'Czerwiec',
            'black'=>'Lipiec',
            'grey'=>'Sierpień',
            'pink'=>'Wrzesień',
            'gold'=>'Październik',
            'silver'=>'Listopad',
            'brown'=>'Grudzień'
            ];
    }
}