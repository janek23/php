<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>LAB1-09-wprowadzenie</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>

<style> td {
        border: 2px solid black;
    } </style>


<b> Zad1 - 09_php_wprowadzenie.pdf</b>
<table>
    <?php for ($i = 0; $i < 10; $i++): ?>
        <?php if ($i % 2 == 0): ?>
            <tr bgcolor="yellow">
        <?php else: ?>
            <tr>
        <?php endif ?>
        <td>Paragraf #<?= $i * 3 ?></td>
        </tr>
    <?php endfor; ?>
</table>

<b> Zad2 - 09_php_wprowadzenie.pdf</b>
<ul>
<?php
$colors = array('gray', 'black', 'yellow', 'green', 'blue', 'red');
foreach($colors as $color) {
    echo "<li style='color:$color'>$color</li>";
}
?>
</ul>



</body>
</html>
