<?php
session_start();

$wrongPass =0;
if (!empty($_POST)) {
    if ($_POST['uzytkownik'] == 'admin' && $_POST['haslo'] == 'tajne') {
        $_SESSION['zalogowany'] = 'tak';
        header("Location: LAB1_pdf2_zad2_tajne.php");
        exit();
    }else {
        $wrongPass = 1;
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WIT</title>
</head>

<body>

<?php if (isset($_GET['komunikat']) && $_GET['komunikat'] == 1) : ?>
    <p style='color: red;'>Najpierw musisz sie zalogowac!</p>
<?php endif; ?>

<?php if ($wrongPass==1) : ?>
    <p style='color: red;'>Podano błędny login lub hasło!</p>
<?php endif; ?>

<form method="post" action="LAB1_pdf2_zad2.php">
    Nazwa użytkownika:
    <input type="text" name="uzytkownik" />
    <br />

    Hasło:
    <input type="password" name="haslo" />
    <br />
    <br />

    <button type="submit">Zaloguj</button>
</form>
</body>

</html>