<?php

namespace Application\Model;

use function Sodium\add;

class Liczby
{
    public function generuj()
    {
        $parzyste= array();
        $nieparzyste= array();

        for($i =0; $i <100; $i++)
        {
            $los = random_int (0 , 1000);

            if($los%2 ==0)
                array_push($parzyste, $los);
            else
                array_push($nieparzyste, $los);
        }

        asort($parzyste);
        asort($nieparzyste);

        return [
            'parzyste' => $parzyste,
            'nieparzyste' => $nieparzyste
        ];
    }
}