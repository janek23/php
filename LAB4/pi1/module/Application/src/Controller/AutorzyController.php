<?php

namespace Application\Controller;

use Application\Form\AutorForm;
use Application\Model\Autor;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class AutorzyController extends AbstractActionController
{
    /**
     * @var Autor
     */
    private $autor;

    /**
     * @var AutorzyForm
     */
    private $autorForm;

    public function __construct(Autor $autor, AutorForm $autorForm)
    {
        $this->autor = $autor;
        $this->autorForm = $autorForm;
    }

    public function listaAction()
    {
        return new ViewModel([
            'autorzy' => $this->autor->pobierzWszystko(),
        ]);
    }

    public function dodajAction()
    {
        $this->autorForm->get('zapisz')->setValue('Dodaj');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->autorForm->setData($request->getPost());

            if ($this->autorForm->isValid()) {
                $this->autor->dodaj($request->getPost());

                return $this->redirect()->toRoute('autorzy');
            }
        }

        return new ViewModel(['nazwisko' => 'Dodawanie Autora', 'form' => $this->autorForm]);
    }

    public function edytujAction()
    {
        $id = (int)$this->params()->fromRoute('id');
        if (empty($id)) {
            $this->redirect()->toRoute('autorzy');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->autorForm->setData($request->getPost());

            if ($this->autorForm->isValid()) {
                $this->autor->aktualizuj($id, $request->getPost());

                return $this->redirect()->toRoute('autorzy');
            }
        } else {
            $daneAutora = $this->autor->pobierz($id);
            $this->autorForm->setData($daneAutora);
        }

        $viewModel = new ViewModel(['nazwisko' => 'Edytuj autora', 'form' => $this->autorForm]);
        $viewModel->setTemplate('application/autorzy/dodaj');

        return $viewModel;
    }

    public function usunAction()
    {
        $id = (int)$this->params()->fromRoute('id');
        if (empty($id)) {
            $this->redirect()->toRoute('autorzy');
        }

        $this->autor->usun($id);

        return $this->redirect()->toRoute('autorzy');
    }

    public function opisAction(){
        $id = (int)$this->params()->fromRoute('id');
        if (empty($id)) {
            $this->redirect()->toRoute('autorzy');
        }

        return new ViewModel(
            $this->autor->pobierz($id)
        );
    }

//    src https://docs.laminas.dev/tutorials/in-depth-guide/data-binding/
    public function usunPOSTAction(){
        $id = (int)$this->params()->fromRoute('id');
        if (empty($id)) {
            $this->redirect()->toRoute('autorzy');
        }

        try {
            $post = $this->autor->pobierz($id);
        } catch (InvalidArgumentException $ex) {
            return $this->redirect()->toRoute('autorzy');
        }

        $request = $this->getRequest();
        if (! $request->isPost()) {
            return new ViewModel(['post' => $post]);
        }

        if ($id != $request->getPost('id')
            || 'Delete' !== $request->getPost('confirm', 'no')
        ) {
            return $this->redirect()->toRoute('autorzy');
        }

        $post = $this->autor->usunPOST($post);
        return $this->redirect()->toRoute('blog');


    }
}
