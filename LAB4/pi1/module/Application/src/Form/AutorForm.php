<?php

namespace Application\Form;

use Application\Model\Ksiazka;
use Laminas\Form\Form;
use Laminas\I18n\Validator\IsFloat;
use Laminas\InputFilter\InputFilterProviderInterface;

class AutorForm extends Form implements InputFilterProviderInterface
{
    public function __construct(Ksiazka $ksiazka)
    {
        parent::__construct('autor');

        $this->setAttributes(['method' => 'post', 'class' => 'form']);

        $this->add([
            'name' => 'imie',
            'type' => 'Text',
            'options' => [
                'label' => 'Imie',
            ],
            'attributes' => ['class' => 'form-control'],
        ]);
        $this->add([
            'name' => 'nazwisko',
            'type' => 'Text',
            'options' => [
                'label' => 'Nazwisko',
            ],
            'attributes' => ['class' => 'form-control'],
        ]);
        $this->add([
            'name' => 'tytul',
            'type' => 'Select',
            'options' => [
                'label' => 'Tytul',
                'value_options' => $ksiazka->pobierzSlownik(),
            ],
            'attributes' => ['class' => 'form-control'],
        ]);
        $this->add([
            'name' => 'opis',
            'type' => 'Textarea',
            'options' => [
                'label' => 'Opis',
            ],
            'attributes' => ['class' => 'form-control'],
        ]);
        $this->add([
            'name' => 'zapisz',
            'type' => 'Submit',
            'attributes' => [
                'value' => 'Zapisz',
                'class' => 'btn btn-primary',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            [
                'name' => 'imie',
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [],
            ],
            [
                'name' => 'nazwisko',
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [],
            ],
            [
                'name' => 'opis',
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [],
            ],
            [
                'name' => 'tytul',
                'required' => false,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [],
            ],
        ];
    }
}