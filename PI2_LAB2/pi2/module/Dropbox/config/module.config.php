<?php

namespace Dropbox;

use Dropbox\Form\DropboxForm;
use Dropbox\Controller\IndexController;
use Dropbox\Controller\IndexControllerFactory;
use Dropbox\Service\Dropbox;
use Laminas\Mvc\Controller\LazyControllerAbstractFactory;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use Laminas\ServiceManager\AbstractFactory\ReflectionBasedAbstractFactory;
use Laminas\ServiceManager\Factory\InvokableFactory;

return [
	'router' => [
        'routes' => [
            'dropbox' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/dropbox',
                    'defaults' => [
                        'controller' => IndexController::class,
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'akcje' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '[/:action]',
                            'defaults' => [
                                'controller' => IndexController::class,
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            Dropbox::class => ReflectionBasedAbstractFactory::class,
            DropboxForm::class => InvokableFactory::class,
        ]
    ],
    'controllers' => [
        'factories' => [
            IndexController::class => LazyControllerAbstractFactory::class,
        ]
    ],
	'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'dropbox' => [
        'key' => 'cl8vbl9b8p94wnx',
        'secret' => '7mjrnvors0t028l'
    ]
];