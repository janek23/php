<?php

namespace Dropbox\Service;

use Laminas\Http\Client;
use Laminas\Json\Json;
use Laminas\Session\Container;

class Dropbox
{
    const API_URL = 'https://api.dropboxapi.com/2/';
    // const REDIRECT_URI = 'http://localhost/pi2/public/dropbox/finish'; // xampp
    const REDIRECT_URI = 'http://localhost:8080/dropbox/finish'; // docker

    private Container $container;
    private array $config;

    public function __construct(array $config)
    {
        $this->container = new Container();
        $this->config = $config['dropbox'];
    }

    public function generateAuthorizationUrl(): string
    {
        return sprintf(
            "https://www.dropbox.com/oauth2/authorize?client_id=%s&redirect_uri=%s&response_type=code",
            $this->config['key'],
            self::REDIRECT_URI
        );
    }

    public function authorized(): bool
    {
        return isset($this->container->access_token);
    }

    public function getAccessToken($authorizationCode)
    {
        $client = new Client('https://api.dropboxapi.com/oauth2/token');
        $client->setMethod('post');
        $client->setParameterPost([
            'code' => $authorizationCode,
            'grant_type' => 'authorization_code',
            'client_id' => $this->config['key'],
            'client_secret' => $this->config['secret'],
            'redirect_uri' => self::REDIRECT_URI,
        ]);

        try {
            $response = $client->send();

            if ($response->isSuccess()) {
                $data = Json::decode($response->getBody());

                if (!empty($data->access_token)) {
                    $this->container->access_token = $data->access_token;
                    return true;
                }
            }

            return "Wystąpił błąd: " . $response->getBody();
        } catch (\Exception $e) {
            return "Wystąpił błąd: " . $e->getMessage();
        }
    }

    public function getFileList($path)
    {
        try {
            $files = $this->sendRequest('/files/list_folder', ['path' => $path]);

            return $files->entries;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function uploadFile($fileName, $content)
    {
        $client = new Client('https://content.dropboxapi.com/2/files/upload');
        $client->setMethod('post');
        $params = [
            "path"=> '/'. $fileName. '.txt',
            "mode"=> "add",
            "autorename"=> false,
            "mute"=> false,
            "strict_conflict"=> false
            ];

        $client->setHeaders([
            'Content-Type'=> 'application/octet-stream',
            'Authorization' => 'Bearer ' . $this->container->access_token,
            'Dropbox-API-Arg' => Json::encode($params),
        ]);

        $client->setRawBody($content);

        try {
            $response = $client->send();

            if ($response->isSuccess()) {
                return "Sukces";
            }
            else
                return "Wystąpił błąd: " . $response->getBody();
        } catch (\Exception $e) {
            return "Wystąpił błąd: " . $e->getMessage();
        }

    }

    public function downloadFile($filePath)
    {
        $client = new Client('https://content.dropboxapi.com/2/files/download');
      //  $client->setMethod('post');
        $client->setHeaders([
            'Authorization' => 'Bearer ' . $this->container->access_token,
            'Dropbox-API-Arg' => Json::encode(array('path' => $filePath)),
        ]);

        try {
            $response = $client->send();

            if ($response->isSuccess()) {
                return $response;
                }
            else
                return "Wystąpił błąd: " . $response->getBody();
        } catch (\Exception $e) {
            return "Wystąpił błąd: " . $e->getMessage();
        }
    }

    public function deleteFile($filePath){

        $client = new Client('https://api.dropboxapi.com/2/files/delete_v2');
        $client->setMethod('post');
        $client->setHeaders([
            'Content-Type'=> 'application/json',
            'Authorization' => 'Bearer ' . $this->container->access_token,
        ]);

        $client->setRawBody(json::encode(array('path'=>$filePath )));

        try {
            $response = $client->send();

            if ($response->isSuccess()) {
                return "Usunieto plik";
            }
            else
                return "Wystąpił błąd: " . $response->getBody();
        } catch (\Exception $e) {
            return "Wystąpił błąd: " . $e->getMessage();
        }

    }

    private function sendRequest($function, $parameters = [])
    {
        $client = new Client(self::API_URL . $function);
        $client->setMethod('post');
        $client->setHeaders([
            'Authorization' => 'Bearer ' . $this->container->access_token,
            'Content-Type' => 'application/json'
        ]);
        $client->setRawBody(Json::encode($parameters));

        $response = $client->send();

        if ($response->isSuccess()) {
            return Json::decode($response->getBody());
        } else {
            throw new \Exception($response->getContent());
        }
    }
}
