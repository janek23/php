<?php

namespace Dropbox\Controller;

use ApiKlient\Form\NieruchomoscForm;
use Dropbox\Form\DropboxForm;
use Dropbox\Service\Dropbox;
use Laminas\Form\Element;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Soap\Client;

class IndexController extends AbstractActionController
{
    private Dropbox $dropbox;
    private DropboxForm $form;

    /**
     * IndexController constructor.
     *
     * @param DropboxForm $form
     * @param Dropbox $drbx
     */
    public function __construct(DropboxForm $form, Dropbox $drbx)
    {
        $this->form = $form;
        $this->dropbox = $drbx;
    }

    public function indexAction()
    {
		if (!$this->dropbox->authorized()) {
			return $this->redirect()->toRoute('dropbox/akcje', ['action' => 'authorize']);
        }

		$path = $this->params()->fromQuery('path', '');
		$files = $this->dropbox->getFileList($path);

        return is_array($files) ? ['files' => $files] : ['msg' => $files];
    }

	public function authorizeAction()
	{
		return ['authorize_url' => $this->dropbox->generateAuthorizationUrl()];
	}

	public function finishAction()
    {
        $code = $this->params()->fromQuery('code');
        $result = $this->dropbox->getAccessToken($code);

        if($result === true) {
            return $this->redirect()->toRoute('dropbox');
        }

        return ['msg' => $result];
    }

    public function dodajAction()
    {
        $request = $this->getRequest();

        $view = [];

        if ($request->isPost()) {
            $dane = $request->getPost();

            $this->form->setData($dane);
            if ($this->form->isValid($dane)) {
                $dane = $this->form->getData();
                $ret = $this->dropbox->uploadFile($dane['nazwa'], $dane['zawartosc']);
                $view['komunikat']['val'] = $ret;
            }
            else {
                $view['komunikat']['danger'] = "blad walidacji formularza";
            }

        }

        $form = $this->form;
        $view['form'] = $form;
        return $view;
    }

    public function pobierzAction()
    {

        $path = $this->params()->fromQuery('path');

        $file = $this->dropbox->downloadFile($path);

        $ret = [
            'filename' =>substr($path,1),
            'data' => $file->getBody()
        ];

        return  $ret;
    }

    public function usunAction()
    {
        $path = $this->params()->fromQuery('path');
        $ret = $this->dropbox->deleteFile($path);
        $view['komunikat']['val'] = $ret;
        return $view;
    }

    public function edytujAction()
    {
        $path = $this->params()->fromQuery('path');
        $request = $this->getRequest();

        $view = [];
        $form = $this->form;

        if ($request->isPost()) {
            $dane = $request->getPost();

            $this->form->setData($dane);
            if ($this->form->isValid($dane)) {
                $dane = $this->form->getData();

                $this->usunAction($path);

                $ret = $this->dropbox->uploadFile(substr(rtrim($path,'.txt'),1), $dane['zawartosc']);
                $view['komunikat']['val'] = $ret;
            }
            else {
                $view['komunikat']['danger'] = "blad walidacji formularza";
            }
        }else{

            $file = $this->dropbox->downloadFile($path);

            $ret = [
                'nazwa' =>substr($path,1),
                'zawartosc' => $file->getBody()
            ];

            $form->setData($ret);
            $form->remove("nazwa");
            $view['nazwa'] = $ret['nazwa'];
        }

        $view['form'] = $form;
        return $view;
    }

}

