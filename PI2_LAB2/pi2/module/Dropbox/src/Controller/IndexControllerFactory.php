<?php

namespace Dropbox\Controller;

use Dropbox\Form\DropboxForm;
use Dropbox\Service\Dropbox;
use Interop\Container\ContainerInterface;

class IndexControllerFactory implements \Laminas\ServiceManager\Factory\FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dropbox = $container->get(Dropbox::class);
        $form = $container->get(DropboxForm::class);
        return new IndexController($form, $dropbox);
    }
}