<?php

namespace Dropbox\Form;

use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;

class DropboxForm extends Form
{
    public function __construct()
    {
        parent::__construct();

        $this->add([
            'type' => 'text',
            'name' => 'nazwa',
            'options' => [
                'label' => 'Nazwa pliku',
            ],
        ]);
        $this->add([
            'type' => 'textarea',
            'name' => 'zawartosc',
            'options' => [
                'label' => 'Zawartosc pliku',
            ],
        ]);
        $this->add([
            'type' => 'submit',
            'name' => 'dodaj',
            'options' => [
                'label' => 'Dodaj/edytuj',
            ],
            'attributes' => [
                'class' => 'btn btn-primary',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            [
                'name' => 'nazwa',
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                ],
            ],
            [
                'name' => 'zawartosc',
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                ],
            ]
        ];
    }


}