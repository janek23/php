<?php

namespace ApiKlient\Controller;

use ApiKlient\Form\NieruchomoscForm;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Soap\Client;
use Laminas\Form\Element;
use function Sodium\add;

class IndexController extends AbstractActionController
{
    private NieruchomoscForm $nieruchomoscForm;
    private Client $klient;
    private string $klucz;

    /**
     * IndexController constructor.
     *
     * @param NieruchomoscForm $nieruchomoscForm
     * @param Client           $klient
     * @param string           $klucz
     */
    public function __construct(NieruchomoscForm $nieruchomoscForm, Client $klient, string $klucz)
    {
        $this->nieruchomoscForm = $nieruchomoscForm;
        $this->klient = $klient;
        $this->klucz = $klucz;
    }

    public function indexAction()
    {
        $request = $this->getRequest();
        $view = [];

        if ($request->isPost()) {
            $dane = $request->getPost();
            $this->nieruchomoscForm->setData($dane);

            if ($this->nieruchomoscForm->isValid()) {
                // dodaj
                try {
                    $id = $this->klient->dodaj($this->klucz, $this->nieruchomoscForm->getData());
                    $view['komunikat']['success'] = "Nieruchomość została dodana (<strong>ID: $id</strong>).";
                } catch (\SoapFault $e) {
                    $view['komunikat']['danger'] = $e->getMessage();
                }
            }
        }

        $form = $this->nieruchomoscForm;//pobierz miasta z serwera

        $miasta = $this->klient->pobierzMiasta($this->klucz);
        $select = new Element\Select('miasto');
        $select->setValueOptions($miasta);
        $miastaArr =[];
        for ($i = 0; $i <sizeof($miasta); $i++) {
            $miastaArr[$i] = $miasta[$i]["miasto"];
        }
        $select->setValueOptions($miastaArr);
        $form->add($select);

        $view['form'] = $form;
        return $view;
    }

    public function listaAction()
    {
        return [
            'dane' => $this->klient->pobierzWszystko($this->klucz),
        ];
    }
    public function usunAction(){

        if( $_GET["id"] )
            $id = $_GET["id"];

        try {
            $id = $this->klient->usun($this->klucz, $id);
            $view['komunikat']['success'] = "Nieruchomość została usunieta";
        } catch (\SoapFault $e) {
            $view['komunikat']['danger'] = $e->getMessage();
        }
        return $view;

    }

    public function edytujAction()
    {
        $request = $this->getRequest();
        $view = [];

        if( $_GET["id"] )
            $id = $_GET["id"];

        if ($request->isPost()) {
            $dane = $request->getPost();
            //print_r($dane);
            $this->nieruchomoscForm->setData($dane);

            if ($this->nieruchomoscForm->isValid()) {
                // edytuj
                try {
                    $id = $this->klient->edytuj($this->klucz, $this->nieruchomoscForm->getData(), $id);
                    $view['komunikat']['success'] = "Nieruchomość została edytowana";
                } catch (\SoapFault $e) {
                    $view['komunikat']['danger'] = $e->getMessage();
                }
            }
        }

        if( $_GET["id"] )
            $id = $_GET["id"];
        $nieruchomosc = $this->klient->pobierzPoID($this->klucz, $id );

        $form = $this->nieruchomoscForm;
        $form->setData($nieruchomosc[0]);

        //pobierz miasta z serwera
        $miasta = $this->klient->pobierzMiasta($this->klucz);
        $select = new Element\Select('miasto');
        $select->setValueOptions($miasta);
        $miastaArr =[];
        for ($i = 0; $i <sizeof($miasta); $i++) {
            $miastaArr[$i] = $miasta[$i]["miasto"];
        }
        print_r($miastaArr);
        $select->setValueOptions($miastaArr);
        $form->add($select);

        $view['form'] = $form;

        return $view;
    }
}