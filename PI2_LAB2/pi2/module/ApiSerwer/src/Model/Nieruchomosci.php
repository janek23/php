<?php

namespace ApiSerwer\Model;

use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Sql\Sql;

class Nieruchomosci
{
    private int $idUzytkownika = 1;

    /**
     * @var Adapter
     */
    private Adapter $adapter;

    private string $klucz;

    /**
     * @param \Laminas\Db\Adapter\Adapter $adapter
     * @param array                       $config
     */
    public function __construct(Adapter $adapter, array $config)
    {
        $this->adapter = $adapter;
        $this->klucz = $config['klucz_api'];
    }

    /**
     * Dodaje nieruchomość.
     *
     * @param string $klucz
     * @param array  $dane
     * @return integer
     * @throws \SoapFault
     */
    public function dodaj(string $klucz, array $dane): int
    {
        $this->sprawdzLogowanie($klucz);

        $sql = new Sql($this->adapter);
        $insert = $sql->insert('nieruchomosci');
        $insert->values([
            'id_uzytkownika' => $this->idUzytkownika,
            'typ_nieruchomosci' => $dane['typ_nieruchomosci'],
            'typ_oferty' => $dane['typ_oferty'],
            'numer' => $dane['numer'],
            'cena' => $dane['cena'],
            'powierzchnia' => str_replace(',', '.', $dane['powierzchnia']),
            'miasto' => $dane['miasto'],
        ]);
        $sqlString = $sql->buildSqlString($insert);
        $wynik = $this->adapter->query($sqlString, Adapter::QUERY_MODE_EXECUTE);

        try {
            return $wynik->getGeneratedValue();
        } catch (\Exception $e) {
            throw new \SoapFault('Server', 'Nie można dodać nieruchomości');
        }
    }

    /**
     * Pobiera listę nieruchomości.
     *
     * @param string $klucz
     * @return array
     * @throws \SoapFault
     */
    public function pobierzWszystko(string $klucz): array
    {
        $this->sprawdzLogowanie($klucz);

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(['n' => 'nieruchomosci']);
        $select->order('n.numer');

        $sqlString = $sql->buildSqlString($select);
        $wynik = $this->adapter->query($sqlString, Adapter::QUERY_MODE_EXECUTE);

        return $wynik->toArray();
    }


    /**
     * Pobiera listę nieruchomości po id.
     *
     * @param string $klucz
     * @param int $id
     * @return array
     * @throws \SoapFault
     */
    public function pobierzPoID(string $klucz, int $id): array
    {
        $this->sprawdzLogowanie($klucz);

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(['n' => 'nieruchomosci']);
        $select->where(['id' =>$id]);

        $sqlString = $sql->buildSqlString($select);
        $wynik = $this->adapter->query($sqlString, Adapter::QUERY_MODE_EXECUTE);

        return $wynik->toArray();
    }

    /**
     * Sprawdza klucz logowania.
     *
     * @param string $klucz
     * @throws \SoapFault
     */
    private function sprawdzLogowanie(string $klucz): void
    {
        if ($klucz != $this->klucz) {
            throw new \SoapFault('Server', 'Brak autoryzacji');
        }
    }

    /**
     * Edytuje nieruchomość.
     *
     * @param string $klucz
     * @param array  $dane
     * @param int $id
     * @return boolean
     * @throws \SoapFault
     */
    public function edytuj(string $klucz, array $dane, int $id): int
    {
        $this->sprawdzLogowanie($klucz);

        $sql = new Sql($this->adapter);
        $update = $sql->update('nieruchomosci');
        $update->where(['id' =>$id]);
        $update->set([
            'id_uzytkownika' => $this->idUzytkownika,
            'typ_nieruchomosci' => $dane['typ_nieruchomosci'],
            'typ_oferty' => $dane['typ_oferty'],
            'numer' => $dane['numer'],
            'cena' => $dane['cena'],
            'powierzchnia' => str_replace(',', '.', $dane['powierzchnia']),
            'miasto' => $dane['miasto'],
        ]);
        $sqlString = $sql->buildSqlString($update);
        $wynik = $this->adapter->query($sqlString, Adapter::QUERY_MODE_EXECUTE);

        try {
            return $wynik->getGeneratedValue();
        } catch (\Exception $e) {
            throw new \SoapFault('Server', 'Nie można edytowac nieruchomości');
        }
    }


    /**
     * Usuwa nieruchomość.
     *
     * @param string $klucz
     * @param int $id
     * @return boolean
     * @throws \SoapFault
     */
    public function usun(string $klucz, int $id): int
    {
        $this->sprawdzLogowanie($klucz);

        $sql = new Sql($this->adapter);
        $rm = $sql->delete('nieruchomosci');
        $rm->where(['id' =>$id]);

        $sqlString = $sql->buildSqlString($rm);
        $wynik = $this->adapter->query($sqlString, Adapter::QUERY_MODE_EXECUTE);

        try {
            return $wynik->getGeneratedValue();
        } catch (\Exception $e) {
            throw new \SoapFault('Server', 'Nie można usunac nieruchomości');
        }
    }

    /**
     * Pobierz miasto
     *
     * @param string $klucz
     * @return array
     * @throws \SoapFault
     */
    public function pobierzMiasta(string $klucz): array
    {
        $this->sprawdzLogowanie($klucz);

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(['n' => 'miasta']);

        $sqlString = $sql->buildSqlString($select);
        $wynik = $this->adapter->query($sqlString, Adapter::QUERY_MODE_EXECUTE);

        return $wynik->toArray();
    }


}