
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `api`
--
CREATE DATABASE `api` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `api`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `nieruchomosci`
--

CREATE TABLE IF NOT EXISTS `nieruchomosci` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_uzytkownika` int(11) NOT NULL,
  `typ_nieruchomosci` enum('mieszkanie','dom','działka') NOT NULL,
  `typ_oferty` enum('sprzedaż','wynajem') NOT NULL,
  `numer` varchar(20) NOT NULL,
  `cena` int(11) NOT NULL,
  `powierzchnia` float NOT NULL,
  `miasto` varchar(100) NOT NULL,
  `data_dodania` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `miasta` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `miasto` varchar(100) NOT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

INSERT INTO miasta (`miasto`)VALUE ("warszawa");
INSERT INTO miasta (`miasto`)VALUE ("krakow");
INSERT INTO miasta (`miasto`)VALUE ("gdansk");

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
