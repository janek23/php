<?php

$client = new \Laminas\Http\Client();
$client->setUri('https://api.flickr.com/services/rest/?format=json&nojsoncallback=1&method=flickr.photos.search&api_key=d50f1948e211129da06068b52266b5c0&text=php');
$response = $client->send();
$photos = \Zend\Json\Json::decode($response->getBody());

// https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}_[mstzb].jpg
$url = "https://farm%s.staticflickr.com/%s/%s_%s_n.jpg";
foreach($photos->photos->photo as $p) {
	echo sprintf($url, $p->farm, $p->server, $p->id, $p->secret);
	echo '<br/>';
}