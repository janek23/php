<?php

define('DB_HOST', 'localhost');
define('DB_NAME', 'googlemaps');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');

function polaczDB(): PDO
{
    return new PDO(
        sprintf('mysql:host=%s;dbname=%s', DB_HOST, DB_NAME),
        DB_USERNAME,
        DB_PASSWORD
    );
}

$pdo = polaczDB();

if( isset($_POST['dl']) &&
    isset($_POST['szer'])&&
    isset($_POST['adr'])&&
    isset($_POST['descr'])
){
    $stmt = $pdo->prepare("INSERT INTO punkt (DlugoscGeograficzna, SzerokoscGeograficzna, Adres, OpisPunktu) VALUES (:dl, :szer, :adr, :descr)");

    $stmt->execute([
        'dl' => $_POST['dl'] ,
        'szer' => $_POST['szer'],
        'adr' => $_POST['adr'],
        'descr' => $_POST['descr']
    ]);

    $auto = $stmt->fetch();
    header('Content-Type: application/json');

    echo json_encode( $auto);

}else {
    echo false;
}
exit();

