<?php

namespace Flickr\Service;

use mysql_xdevapi\Exception;

class Flickr
{
    const DEV_KEY = '21d42c03ef848f005c4aacd30f996815';
    const SECRET = '0770e715155bd775';

    private $client;

    public function __construct()
    {
        $this->client = new \Laminas\Http\Client();
    }

    public function GetPhotosList()
    {
        $this->client->setUri('https://api.flickr.com/services/rest/?format=json&nojsoncallback=1&method=flickr.photos.search&api_key=21d42c03ef848f005c4aacd30f996815&text=php&per_page=5');
        $response = $this->client->send();
        $photos = \Zend\Json\Json::decode($response->getBody());

        return $this->parseResponseToTuplesArray($photos);
    }

    public function Search($query = null, $user_id = null, $per_page = 5)
    {
        $key = self::DEV_KEY;
        $uri = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key={$key}&text={$query}&per_page={$per_page}&format=json&nojsoncallback=1";
        $this->client->setUri($uri);
        $response = $this->client->send();
        $result = \Zend\Json\Json::decode($response->getBody());
        return $this->parseResponseToTuplesArray($result);
    }

    public function AllUserPhotos($user_id, $per_page =5){
        $key = self::DEV_KEY;
        $uri = "https://api.flickr.com/services/rest/?method=flickr.people.getPhotos&api_key={$key}&user_id={$user_id}&per_page={$per_page}&format=json&nojsoncallback=1";
        $this->client->setUri($uri);
        $response = $this->client->send();
        $result = \Zend\Json\Json::decode($response->getBody());
        return $this->parseResponseToTuplesArray($result);
    }

    private function parseResponseToTuplesArray($response)
    {
        $urls =[];
        foreach ($response->photos->photo as $photo)
        {
            $tuple =[];
            $photoURL="https://farm{$photo->farm}.staticflickr.com/{$photo->server}/{$photo->id}_{$photo->secret}.jpg";

            //full size (uneffective!)
            $key = self::DEV_KEY;
            $uri = "https://api.flickr.com/services/rest/?method=flickr.photos.getSizes&api_key={$key}&photo_id={$photo->id}&format=json&nojsoncallback=1";
            $this->client->setUri($uri);
            $responseFullSize = $this->client->send();
            try {
                $result = \Zend\Json\Json::decode($responseFullSize->getBody());
            }catch(Exception $ex)
            {

            }
            $fullsize = end($result->sizes->size);
            $fullSizeUrl = $fullsize->url;

            array_push($tuple, $photoURL);
            array_push($tuple, $fullSizeUrl);
            array_push($urls, $tuple);
        }
        return $urls;
    }


}
