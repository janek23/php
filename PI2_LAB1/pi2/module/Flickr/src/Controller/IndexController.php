<?php

namespace Flickr\Controller;

use Flickr\Service\Flickr;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use function Sodium\add;


class IndexController extends AbstractActionController
{
    private $flickrService;

    public function __construct(Flickr $flickrService)
    {
        $this->flickrService = $flickrService;
    }

    public function indexAction()
    {
        $urls = $this->flickrService->GetPhotosList();

        return ['urls'=>$urls];
    }

    public function searchAction()
    {
        $phrase = $this->params()->fromQuery('phrase');
        $view = new ViewModel(['phrase' => $phrase]);
        if(!empty($phrase)) {
            $urls = $this->flickrService->Search($phrase);
            $listView = new ViewModel(['urls' => $urls, 'phrase' => $phrase, 'title' => 'wyszukano: ' . $phrase, 'action' => 'search']);
            $listView->setTemplate('flickr/index/index');

            $view->addChild($listView, 'list');
        }

        return $view;
    }

    public function alluserphotosAction()
    {
        $userId = $this->params()->fromQuery('userId');
        $view = new ViewModel(['userId' => $userId]);
        if(!empty($userId)) {
            $urls = $this->flickrService->AllUserPhotos($userId);
            $listView = new ViewModel(['urls' => $urls, 'userId' => $userId, 'title' => 'userId: ' . $userId, 'action' => 'alluserphotos']);
            $listView->setTemplate('flickr/index/index');

            $view->addChild($listView, 'list');
        }

        return $view;
    }






}